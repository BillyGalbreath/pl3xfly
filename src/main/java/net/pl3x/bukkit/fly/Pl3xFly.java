package net.pl3x.bukkit.fly;

import net.pl3x.bukkit.fly.commands.CmdFly;
import net.pl3x.bukkit.fly.commands.CmdFlySpeed;
import net.pl3x.bukkit.fly.commands.CmdPl3xFly;
import net.pl3x.bukkit.fly.configuration.Config;
import net.pl3x.bukkit.fly.configuration.Lang;
import net.pl3x.bukkit.fly.listener.PlayerListener;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class Pl3xFly extends JavaPlugin {
    @Override
    public void onEnable() {
        Config.reload(this);
        Lang.reload(this);

        getServer().getPluginManager().registerEvents(new PlayerListener(), this);

        getCommand("fly").setExecutor(new CmdFly(this));
        getCommand("flyspeed").setExecutor(new CmdFlySpeed(this));
        getCommand("pl3xfly").setExecutor(new CmdPl3xFly(this));

        getLogger().info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    public void onDisable() {
        getLogger().info(getName() + " disabled.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        sender.sendMessage(ChatColor.DARK_RED + getName() + " is disabled. Please check console logs for more information.");
        return true;
    }
}
