package net.pl3x.bukkit.fly.commands;

import net.pl3x.bukkit.fly.Pl3xFly;
import net.pl3x.bukkit.fly.configuration.Lang;
import org.apache.commons.lang.BooleanUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Collectors;

public class CmdFly implements TabExecutor {
    private final Pl3xFly plugin;

    public CmdFly(Pl3xFly plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            return Bukkit.getOnlinePlayers().stream()
                    .filter(player -> player.getName().toLowerCase().startsWith(args[0].toLowerCase()))
                    .map(Player::getName).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.fly")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        boolean other = false;
        Player target;

        if (args.length > 0) {
            if (!sender.hasPermission("command.fly.others")) {
                Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
                return true;
            }
            //noinspection deprecation
            target = Bukkit.getPlayer(args[0]);
            if (target == null) {
                Lang.send(sender, Lang.PLAYER_NOT_FOUND);
                return true;
            }
            if (!target.hasPermission("command.fly")) {
                Lang.send(sender, Lang.TARGET_NO_PERMISSION);
                return true;
            }
            if (target.hasPermission("command.fly.exempt")) {
                Lang.send(sender, Lang.FLY_EXEMPT);
                return true;
            }
            other = true;
        } else {
            if (!(sender instanceof Player)) {
                Lang.send(sender, Lang.PLAYER_COMMAND);
                return true;
            }
            target = (Player) sender;
        }

        target.setAllowFlight(!target.getAllowFlight());

        if (!target.getAllowFlight()) {
            target.setFlying(false);
            target.setFlySpeed(0.1F);
            target.setFallDistance(-256);
        }

        String action = BooleanUtils.toStringOnOff(target.getAllowFlight());
        Lang.send(sender, (other ? Lang.FLY_SET_OTHER : Lang.FLY_SET)
                .replace("{action}", action)
                .replace("{target}", target.getName()));
        if (other) {
            Lang.send(target, Lang.FLY_SET
                    .replace("{action}", action));
        }
        return true;
    }
}
