package net.pl3x.bukkit.fly.commands;

import net.pl3x.bukkit.fly.Pl3xFly;
import net.pl3x.bukkit.fly.configuration.Lang;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Collectors;

public class CmdFlySpeed implements TabExecutor {
    private final Pl3xFly plugin;

    public CmdFlySpeed(Pl3xFly plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            return Bukkit.getOnlinePlayers().stream()
                    .filter(player -> player.getName().toLowerCase().startsWith(args[0].toLowerCase()))
                    .map(Player::getName).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.flyspeed")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length == 0) {
            Lang.send(sender, Lang.MUST_SPECIFY_SPEED);
            return true;
        }

        boolean other = false;
        Player target;
        String rawNum;

        if (args.length > 1) {
            if (!sender.hasPermission("command.flyspeed.others")) {
                Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
                return true;
            }
            //noinspection deprecation
            target = Bukkit.getPlayer(args[0]);
            if (target == null) {
                Lang.send(sender, Lang.PLAYER_NOT_FOUND);
                return true;
            }
            if (!target.hasPermission("command.flyspeed")) {
                Lang.send(sender, Lang.TARGET_NO_PERMISSION);
                return true;
            }
            if (target.hasPermission("command.flyspeed.exempt")) {
                Lang.send(sender, Lang.FLYSPEED_EXEMPT);
                return true;
            }
            rawNum = args[1];
            other = true;
        } else {
            if (!(sender instanceof Player)) {
                Lang.send(sender, Lang.PLAYER_COMMAND);
                return true;
            }
            target = (Player) sender;
            rawNum = args[0];
        }


        float flySpeed;
        try {
            flySpeed = Float.parseFloat(rawNum);
        } catch (NumberFormatException e) {
            Lang.send(sender, Lang.INVALID_NUMBER);
            return true;
        }

        if (flySpeed < 0 || flySpeed > 10) {
            Lang.send(sender, Lang.INVALID_NUMBER);
            return true;
        }

        if (!sender.hasPermission("command.flyspeed.limit.*") && failedLimit(target, flySpeed)) {
            Lang.send(sender, (other ? Lang.BEYOND_LIMIT_OTHER : Lang.BEYOND_LIMIT)
                    .replace("{speed}", Float.toString(flySpeed))
                    .replace("{target}", target.getName()));
            return true;
        }

        target.setFlySpeed(flySpeed / 10F);

        Lang.send(sender, (other ? Lang.FLYSPEED_SET_OTHER : Lang.FLYSPEED_SET)
                .replace("{speed}", Float.toString(flySpeed))
                .replace("{target}", target.getName()));
        return true;
    }

    private boolean failedLimit(Player player, float speed) {
        for (int i = (int) Math.ceil(speed); i <= 10; i++) {
            if (player.hasPermission("command.flyspeed.limit." + i)) {
                return false;
            }
        }
        return true;
    }
}
