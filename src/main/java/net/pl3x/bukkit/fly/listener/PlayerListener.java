package net.pl3x.bukkit.fly.listener;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import static org.bukkit.event.EventPriority.MONITOR;

public class PlayerListener implements Listener {
    @EventHandler(priority = MONITOR)
    public void onPlayerJoin(PlayerJoinEvent event) {
        resetFly(event.getPlayer());
    }

    @EventHandler(priority = MONITOR)
    public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
        resetFly(event.getPlayer());
    }

    private void resetFly(Player player) {
        if (player.hasPermission("command.fly")) {
            player.setAllowFlight(true);
            player.setFlySpeed(0.1F);
            if (!player.isOnGround()) {
                player.setFlying(true);
            }
            return;
        }

        if (player.getGameMode() == GameMode.SPECTATOR ||
                player.getGameMode() == GameMode.ADVENTURE) {
            player.setAllowFlight(false);
            player.setFlying(false);
            player.setFallDistance(-256);
        }
        player.setFlySpeed(0.1F);
    }
}
