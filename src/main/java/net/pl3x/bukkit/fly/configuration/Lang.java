package net.pl3x.bukkit.fly.configuration;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class Lang {
    public static String COMMAND_NO_PERMISSION;
    public static String PLAYER_COMMAND;
    public static String PLAYER_NOT_FOUND;
    public static String TARGET_NO_PERMISSION;

    public static String MUST_SPECIFY_SPEED;
    public static String INVALID_NUMBER;

    public static String BEYOND_LIMIT;
    public static String BEYOND_LIMIT_OTHER;

    public static String FLY_EXEMPT;
    public static String FLYSPEED_EXEMPT;

    public static String FLY_SET;
    public static String FLY_SET_OTHER;

    public static String FLYSPEED_SET;
    public static String FLYSPEED_SET_OTHER;

    public static String VERSION;
    public static String RELOAD;

    private Lang() {
    }

    public static void reload(JavaPlugin plugin) {
        String langFile = Config.LANGUAGE_FILE;
        File configFile = new File(plugin.getDataFolder(), langFile);
        plugin.saveResource(Config.LANGUAGE_FILE, false);
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        COMMAND_NO_PERMISSION = config.getString("command-no-permission", "&4You do not have permission for that command");
        PLAYER_COMMAND = config.getString("player-command", "&4Player only command!");
        PLAYER_NOT_FOUND = config.getString("player-not-found", "&4Player not online!");
        TARGET_NO_PERMISSION = config.getString("target-no-permission", "&4That player does not have permission for that");

        MUST_SPECIFY_SPEED = config.getString("must-specify-speed", "&4Must specify fly speed!");
        INVALID_NUMBER = config.getString("invalid-number", "&4Invalid number specified!");

        BEYOND_LIMIT = config.getString("beyond-number", "&4That speed is beyond your allowed limit!");
        BEYOND_LIMIT_OTHER = config.getString("beyond-limit-other", "&4That speed is beyond {target}'s allowed limit!");

        FLY_EXEMPT = config.getString("fly-exempt", "&4You cannot toggle that player's fly mode!");
        FLYSPEED_EXEMPT = config.getString("flyspeed-exempt", "&4You cannot change that player's fly speed!");

        FLY_SET = config.getString("fly-set", "&dFly mode toggled &7{action}");
        FLY_SET_OTHER = config.getString("fly-set-other", "&dFly mode toggled &7{action} &dfor &7{target}");

        FLYSPEED_SET = config.getString("flyspeed-set", "&dFly speed set to &7{speed}");
        FLYSPEED_SET_OTHER = config.getString("flyspeed-set-other", "&dFly speed set to &7{speed} &dfor &7{target}");

        VERSION = config.getString("version", "&d{plugin} v{version}");
        RELOAD = config.getString("reload", "&d{plugin} v{version} reloaded");
    }

    public static void send(CommandSender recipient, String message) {
        if (message == null) {
            return; // do not send blank messages
        }
        message = ChatColor.translateAlternateColorCodes('&', message);
        if (ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\n")) {
            recipient.sendMessage(part);
        }
    }
}
